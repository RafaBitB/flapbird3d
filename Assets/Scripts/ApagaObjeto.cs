﻿using UnityEngine;
using System.Collections;

public class ApagaObjeto : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Invoke("destroiObjeto",1.5f);
	}
	
	// Update is called once per frame
	void destroiObjeto()
    {
        Destroy(this.gameObject);
	}
}
