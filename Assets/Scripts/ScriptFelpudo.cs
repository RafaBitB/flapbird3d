﻿using UnityEngine;
using System.Collections;

public class ScriptFelpudo : MonoBehaviour {


    public GameObject cameraPrincipal;

    public AudioClip somBate;
    public AudioClip somPonto;
    public AudioClip somVoa;

    void OnTriggerEnter (Collider objeto) {
	
        if(objeto.gameObject.tag == "Finish")
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().velocity = new Vector3(-0.0f, 8.5f, -10.0f);
            GetComponent<Rigidbody>().AddTorque(new Vector3(-100.0f, -100.0f, -100.0f));

            cameraPrincipal.SendMessage("FimDeJogo");
            GetComponent<AudioSource>().PlayOneShot(somBate);
        }
	}
	
	void OnTriggerExit(Collider objeto) {

        if (objeto.gameObject.tag == "GameController")
        {
            Destroy(objeto.gameObject);
            cameraPrincipal.SendMessage("MarcaPonto");
            GetComponent<AudioSource>().PlayOneShot(somPonto);
        }
        
    }
    void SomVoa()
    {
        GetComponent<AudioSource>().PlayOneShot(somVoa);
    }
}
