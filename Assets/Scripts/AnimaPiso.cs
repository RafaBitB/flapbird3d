﻿using UnityEngine;
using System.Collections;

public class AnimaPiso : MonoBehaviour {

    public Material materialPiso;
    private float velocidade = 0.75f;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //Time.time é um valor que vai contado a partir do momento que o jogo começou.
        float offSet = Time.time * velocidade;
        materialPiso.SetTextureOffset("_MainTex",new Vector2(offSet,0));
	}
}
