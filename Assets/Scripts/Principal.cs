﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Principal : MonoBehaviour {

    public GameObject cerca;
    public GameObject arbusto;
    public GameObject nuvem;
    public GameObject pedra;
    public GameObject canos;
    public GameObject particulaPenas;
    public GameObject jogadorFelpudo;

    bool comecou;
    bool terminou;

    public Text textoScore;
    public GameObject luz;
    int pontuacao;

    void Start()
    {
        Physics.gravity = new Vector3(0, -20.0f,0);
        textoScore.text = "Toque para iniciar!";
        
    }
    void Update () {

        if (Input.anyKeyDown && !terminou)
        {
            if (!comecou)
            {
                comecou = true;
                //Invoca repetidas vezes a função CriaCerca, espera um 1seg, e a cada 0.4seg chama a função;
                InvokeRepeating("CriaCerca", 1, 0.42f);
                InvokeRepeating("CriaObjetos", 1, 0.75f);

                jogadorFelpudo.GetComponent<Rigidbody>().useGravity = true;
                jogadorFelpudo.GetComponent<Rigidbody>().isKinematic = false;

                textoScore.text = pontuacao.ToString();
                textoScore.fontSize = 70;
            }
            VoaFelpudo();
        }
        luz.transform.rotation = Quaternion.Euler((luz.transform.rotation.x)+ (pontuacao*2), (luz.transform.rotation.y)+(pontuacao*5), (luz.transform.rotation.z)+ (pontuacao * 2));
        //Para o personagem dar um empinada com relação a velicidade que é aplicada em seu eixo Y;
        jogadorFelpudo.transform.rotation = Quaternion.Euler(jogadorFelpudo.GetComponent<Rigidbody>().velocity.y*-3,0,0);
    }

    private void VoaFelpudo()
    {
        GameObject novaParticula = Instantiate(particulaPenas);
        novaParticula.transform.position = jogadorFelpudo.transform.position;

        jogadorFelpudo.GetComponent<Rigidbody>().velocity = Vector3.zero;
        jogadorFelpudo.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 7.5f, 0.0f);
        jogadorFelpudo.SendMessage("SomVoa");
    }

    // Update is called once per frame
    void CriaCerca()
    {
        Instantiate(cerca);
	}
    void CriaObjetos()
    {
        int sorteiaObjeto = UnityEngine.Random.Range(1, 6);

        float posicaoXrandom = UnityEngine.Random.Range(-2.13f, 2.71f);
        float posicaoYrandom = UnityEngine.Random.Range(1.64f,3f);
        float rotacaoYrandom = UnityEngine.Random.Range(0.0f, 360.0f);

        GameObject novoObjeto = new GameObject();


        switch (sorteiaObjeto)
        {
            case 1:
                {
                    novoObjeto = Instantiate(arbusto);
                    posicaoYrandom = novoObjeto.transform.position.y;
                }
                break;
            case 2: novoObjeto = Instantiate(nuvem); break;
            case 3:
                {
                    novoObjeto = Instantiate(pedra);
                    posicaoYrandom = novoObjeto.transform.position.y;
                }
                break;
            case 4:
                {
                    novoObjeto = Instantiate(canos);
                    posicaoYrandom = UnityEngine.Random.Range(-2.5f, -1.8f);
                    posicaoXrandom = novoObjeto.transform.position.x;
                }
                break;
            case 5:
                {
                    novoObjeto = Instantiate(canos);
                    posicaoYrandom = novoObjeto.transform.position.y;
                    posicaoXrandom = novoObjeto.transform.position.x;
                }
                break;
            default: break;
        }

        novoObjeto.transform.position = new Vector3(posicaoXrandom,posicaoYrandom,novoObjeto.transform.position.z);
        novoObjeto.transform.rotation = Quaternion.Euler(novoObjeto.transform.rotation.x,rotacaoYrandom, novoObjeto.transform.rotation.z);
        
    }
    void FimDeJogo()
    {
        terminou = true;
        CancelInvoke("CriaCerca");
        CancelInvoke("CriaObjetos");
        Invoke("RecarregaCena", 1);
    }
    void MarcaPonto()
    {
        pontuacao++;
        textoScore.text = pontuacao.ToString();
    }
    void RecarregaCena()
    {
        Application.LoadLevel("Cena01");
    }
}
